let students = [];
let credits = [4, 7, 6, 3];

students[0] = {
  firstName: "Jean",
  lastName: "renoir",
  age: 26,
  scores: {
    javascript: 62,
    react: 57,
    python: 88,
    java: 90,
  },
};

students[0].scoreSum =
  students[0].scores.javascript +
  students[0].scores.react +
  students[0].scores.python +
  students[0].scores.java;
students[0].scoreAverage =
  (students[0].scores.javascript +
    students[0].scores.react +
    students[0].scores.python +
    students[0].scores.java) /
  4;
students[0].gpa =
  (1 * credits[0] + 0.5 * credits[1] + 3 * credits[2] + 3 * credits[3]) /
  (credits[0] + credits[1] + credits[2] + credits[3]);

console.log(students[0]);

students[1] = {
  firstName: "Claude",
  lastName: "Monet",
  age: 19,
  scores: {
    javascript: 77,
    react: 52,
    python: 92,
    java: 67,
  },
};
students[1].scoreSum =
  students[1].scores.javascript +
  students[1].scores.react +
  students[1].scores.python +
  students[1].scores.java;
students[1].scoreAverage =
  (students[1].scores.javascript +
    students[1].scores.react +
    students[1].scores.python +
    students[1].scores.java) /
  4;
students[1].gpa =
  (2 * credits[0] + 0.5 * credits[1] + 4 * credits[2] + 1 * credits[3]) /
  (credits[0] + credits[1] + credits[2] + credits[3]);

console.log(students[1]);

students[2] = {
  firstName: "Van",
  lastName: "Gogh",
  age: 21,
  scores: {
    javascript: 51,
    react: 98,
    python: 65,
    java: 70,
  },
};

students[2].scoreSum =
  students[2].scores.javascript +
  students[2].scores.react +
  students[2].scores.python +
  students[2].scores.java;
students[2].scoreAverage =
  (students[2].scores.javascript +
    students[2].scores.react +
    students[2].scores.python +
    students[2].scores.java) /
  4;
students[2].gpa =
  (2 * credits[0] + 4 * credits[1] + 1 * credits[2] + 1 * credits[3]) /
  (credits[0] + credits[1] + credits[2] + credits[3]);

console.log(students[2]);

students[3] = {
  firstName: "Dam",
  lastName: "Square",
  age: 36,
  scores: {
    javascript: 82,
    react: 53,
    python: 80,
    java: 65,
  },
};

console.log(students[3]);

students[3].scoreSum =
  students[3].scores.javascript +
  students[3].scores.react +
  students[3].scores.python +
  students[3].scores.java;
students[3].scoreAverage =
  (students[3].scores.javascript +
    students[3].scores.react +
    students[3].scores.python +
    students[3].scores.java) /
  4;
students[3].gpa =
  (3 * credits[0] + 0.5 * credits[1] + 2 * credits[2] + 1 * credits[3]) /
  (credits[0] + credits[1] + credits[2] + credits[3]);

//problem 3

let wholeAverage = (scoreSum0 + students[0].scoreSum + scoreSum2 + scoreSum3) / 16;

student[0].scoreAverage > wholeAverage
  ? console.log("Monet Has The Red Diploma")
  : console.log("Monet Is The Enemy Of Our Country");
student[1].scoreAverage > wholeAverage
  ? console.log("Renoir Has The Red Diploma")
  : console.log("Renoir Is The Enemy Of Our Country");
student[2].scoreAverage > wholeAverage
  ? console.log("Gogh Has The Red Diploma")
  : console.log("Gogh Is The Enemy Of Our Country");
student[3].scoreAverage > wholeAverage
  ? console.log("Square Has The Red Diploma")
  : console.log("Square Is The Enemy Of Our Country");

// problem 4
if (students[0].gpa > students[1].gpa && students[0].gpa > students[3].gpa && students[1].gpa > students[3].gpa) {
  console.log(`${students[0].firstName} Has The Highest GPA`);
} else if (students[1].gpa > student[1].gpa && students[1].gpa > students[2].gpa && students[1].gpa > students[3].gpa) {
  console.log(`${students[1].firstName} Has The Highest GPA`);
} else if (students[2].gpa > students[1].gpa && students[2].gpa > students[1].gpa && students[2].gpa > student[3].gpa) {
  console.log(`${students[2].firstName} Has The Highest GPA`);
} else {
  console.log(`${students[3].firstName} Has The Highest GPA`);
}

//problem 5

if (
  (student[0].scoreAverage > student[1].scoreAverage &&
    students[0].age >= 21 &&
    students[1].age >= 21) ||
  (student[0].scoreAverage > student[2].scoreAverage &&
    students[0].age >= 21 &&
    students[2].age >= 21) ||
  (student[0].scoreAverage > student[3].scoreAverage &&
    students[0].age >= 21 &&
    students[3].age >= 21)
) {
  console.log("Renoir Has The Best Average Among 21+");
} else if (
  (student[1].scoreAverage > student[0].scoreAverage &&
    students[1].age >= 21 &&
    students[0].age >= 21) ||
  (student[1].scoreAverage > student[2].scoreAverage &&
    students[1].age >= 21 &&
    students[2].age >= 21) ||
  (student[1].scoreAverage > student[3].scoreAverage &&
    students[1].age >= 21 &&
    students[3].age >= 21)
) {
  console.log("Monet Has The Best Average Among 21+");
} else if (
  (student[2].scoreAverage > student[0].scoreAverage &&
    students[2].age >= 21 &&
    students[0].age >= 21) ||
  (student[2].scoreAverage > student[1].scoreAverage &&
    students[2].age >= 21 &&
    students[2].age >= 21) ||
  (student[1].scoreAverage > student[3].scoreAverage &&
    students[2].age >= 21 &&
    students[3].age >= 21)
) {
  console.log("Gogh Has The Best Average Among 21+");
} else {
  console.log("Square Has The Best Average Among 21+");
}

// problem 6

let monetFrontEndScore = (students[1].javascript + students[1].react) / 2;

let renoirfrontEndScore = (students[0].javascript + students[0].react) / 2;

let goghfrontEndScore = (students[2].javascript + students[2].react) / 2;

let squarefrontEndScore = (students[3].javascript + students[3].react) / 2;

if (
  monetFrontEndScore > renoirfrontEndScore &&
  monetFrontEndScore > goghfrontEndScore &&
  monetFrontEndScore > squarefrontEndScore
) {
  console.log(`${students[1].firstName} is the best front end developer`);
} else if (
  renoirfrontEndScore > monetFrontEndScore &&
  renoirfrontEndScore > goghfrontEndScore &&
  renoirfrontEndScore > squarefrontEndScore
) {
  console.log(`${students[0].firstName} is the best front end developer`);
} else if (
  goghfrontEndScore > monetFrontEndScore &&
  goghfrontEndScore > renoirfrontEndScore &&
  goghfrontEndScore > squarefrontEndScore
) {
  console.log(`${students[2].firstName} is the best front end developer`);
} else {
  console.log(`${students[3].firstName} is the best front end developer`);
}
